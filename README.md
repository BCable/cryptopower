cryptopower 1.0.0
=================

ZSH prompt string for usage with multiple servers.  Uses an algorithm to produce the same color every time for every hostname provided.

## Requirements

* bc
* powerline
* zsh

## Usage

Easy install is to copy to ~/.zshrc.cryptopower, then in your ~/.zshrc:

    source ~/.zshrc.cryptopower

Beforehand you can include overrides for the two global variables which specify the main users not to color:

    export CRYPTOPOWER_UID=(1000)
    export CRYPTOPOWER_USER=(jack jill)

## Acknowledgements

Loosely based on [agnoster](https://gist.github.com/agnoster/3712874)'s theme for oh-my-zsh.
